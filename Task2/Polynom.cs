using System;
using System.Text;


public class Polynom
{
    public double[] Nodes;

    //default constructor
    public Polynom()
    {
        Nodes = null;
    }

    //constructor with power and array of indexes (0-index is included)
    public Polynom(int pow, params double[] masDoubles)
    {
        try
        {
            Nodes = new double[pow + 1];

            for (int i = 0; i < masDoubles.Length; i++)
            {
                Nodes[i] = masDoubles[i];
            }
        }
        catch (IndexOutOfRangeException)
        {
            Console.WriteLine("Error in declaring polynom.");
            Nodes = new double[pow + 1];
        }
    }

    public double CountPolynom(double arg)
    {
        double result = 0;

        //cycle for adding every part of polynom
        for (int i = 0; i < Nodes.Length; i++)
        {
            if (Nodes[i] != 0)
            {
                result += Nodes[i]*Math.Pow(arg, i);
            }
        }

        return result;
    }

    public override string ToString()
    {
        StringBuilder outputedPolynom = new StringBuilder();
        if (Nodes[0] != 0)
        {
            outputedPolynom.Append(Nodes[0]);
        }
        for (int i = 1; i < Nodes.Length; i++)
        {
            if (Nodes[i] != 0)
            {
                if (Nodes[i] < 0)
                {
                    outputedPolynom.Append(Nodes[i] + "a^" + i);
                }
                else
                {
                    if (Nodes[i] > 0 && outputedPolynom.ToString() != "")
                    {
                        outputedPolynom.Append("+" + Nodes[i] + "a^" + i);
                    }
                    else
                    {
                        outputedPolynom.Append(Nodes[i] + "a^" + i);
                    }
                }
            }
        }
        //returning statement
        if (outputedPolynom.ToString() == "")
        {
            return "No arguments inputed.";
        }
        return outputedPolynom.ToString();
    }

    public static Polynom operator +(Polynom firstArgument, Polynom secondArgument)
    {
        Polynom resultPolynom = new Polynom(Math.Max(firstArgument.Nodes.Length, secondArgument.Nodes.Length));

        //cycle throught first polynom
        for (int i = 0; i < firstArgument.Nodes.Length; i++)
        {
            resultPolynom.Nodes[i] += firstArgument.Nodes[i];
        }

        //cycle through second polynom
        for (int i = 0; i < secondArgument.Nodes.Length; i++)
        {
            resultPolynom.Nodes[i] += secondArgument.Nodes[i];
        }

        return resultPolynom;
    }

    public static Polynom operator -(Polynom firstArgument, Polynom secondArgument)
    {
        Polynom resultPolynom = new Polynom(Math.Max(firstArgument.Nodes.Length, secondArgument.Nodes.Length));

        //cycle throught first polynom
        for (int i = 0; i < firstArgument.Nodes.Length; i++)
        {
            resultPolynom.Nodes[i] += firstArgument.Nodes[i];
        }

        //cycle through second polynom
        for (int i = 0; i < secondArgument.Nodes.Length; i++)
        {
            resultPolynom.Nodes[i] -= secondArgument.Nodes[i];
        }

        return resultPolynom;
    }

    public static Polynom operator *(Polynom firstArgument, Polynom secondArgument)
    {
        Polynom resultPolynom = new Polynom(firstArgument.Nodes.Length + secondArgument.Nodes.Length - 1);

        for (int i = 0; i < firstArgument.Nodes.Length; i++)
        {
            for (int j = 0; j < secondArgument.Nodes.Length; j++)
            {
                resultPolynom.Nodes[i + j] += firstArgument.Nodes[i]*secondArgument.Nodes[j];
            }
        }

        return resultPolynom;
    }
}

public class Program
{
    static void Main(string[] args)
    {
        //declare polynoms
        Polynom firstTestPolynom = new Polynom(5, 7, 8, -9, 0, 8);
        Polynom secondTestPolynom = new Polynom(9, 0, 2.9, 8, 0, 7, -4, 8, 76);
       
        //output polynoms
        Console.WriteLine("First polynom = {0}", firstTestPolynom);
        Console.WriteLine("Second polynom ={0} \n", secondTestPolynom);

        //output the results of math operations
        Console.WriteLine("Operation +:\n\t" + (firstTestPolynom + secondTestPolynom));
        Console.WriteLine("Operation -:\n\t" + (firstTestPolynom - secondTestPolynom));
        Console.WriteLine("Operation *:\n\t" + (firstTestPolynom*secondTestPolynom));

        //operation with argument
        double arg = Math.Round(new Random().NextDouble(),2);
        Console.WriteLine("\nArgument: {0}\nResults:\n\tfirst polynom = {1}\n\tsecond polynom = {2}.", arg, firstTestPolynom.CountPolynom(arg), secondTestPolynom.CountPolynom(arg));
        
        Console.Read();
    }
}